const express = require('express')
const app = express();
const PORT = 8002;
app.get("/", (req, res) => {
    res.send("This is our Home page");
});
app.get("/about", (req, res) => {
    res.send("This is our About us page");
});
app.get("/contact", (req, res) => {
    res.send("This is our Contact us page");
});
app.get("/temp", (req, res) => {
    res.send("This is our Temprature page");
});
app.get("/*", (req, res) => {
    res.send("404 Page not found");
});

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});