const path = require("path");
const express = require('express')
const app = express();
const port = process.env.PORT || 8001;
const staticPath = path.join(__dirname, "../public");
const templatePath = path.join(__dirname, "../templates");
app.set('view engine', 'hbs');
app.set('views', templatePath);
// app.use(express.static(staticPath));
app.get("/", (req, res) => {
    res.render('index');
});
app.get("/", (req, res) => {
    res.send("This is our Home1 page");
});
app.get("/about", (req, res) => {
    res.send("This is our About us page");
});
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});